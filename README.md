## Raylib-D

This is a package for interacting with the official Raylib dynamic library. [v4.0.0]

All interaction operates like its natural C counterpart, and is bound using dynalib to runtime-load conveniently.

Referenced source (includes folder packaged with releases):
https://github.com/raysan5/raylib

Optional:

Clone precompiled binaries and bind configuration file to output directory by adding the sub-package "copy-files" to your dependencies.

Static binding by adding the following line to your dub.
"versions": [ "SL_RAYLIB" ]